import { createStore } from 'vuex';
import marketModule from './market/market-module'

const Store = createStore({
    modules: {
        market: marketModule
    }
})

export default Store;