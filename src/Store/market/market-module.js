import axios from 'axios';

const marketModule = {
    namespaced: true,
    state() {
        return {
            mmarketData: {},
            loading: true,
            error: [false, ''],
            refreshTime: 15000 //ms
        }
    },
    mutations: {
        updateMarketData(state, payload) {
            state.marketData = payload;
        },
        updateLoading(state) {
            state.loading = false;
        },
        setError(state, payload) {
            state.error = [true, payload]
        },
        setRefresh(state, payload) {
            state.refreshTime = payload[0];
        }
    },
    getters: {
        marketData(state) {
            return state.marketData;
        },
        getStatus(state) {
            return state.loading;
        },
        getError(state) {
            return state.error;
        },
        getRefreshTime(state) {
            return state.refreshTime;
        }
    },
    actions: {
        async getMarketData(context) {
            try {
                const request = await axios.get('https://liquality.io/swap/agent/api/swap/marketinfo');
                context.commit('updateMarketData', request.data);
                context.commit('updateLoading');
            } catch (error) {
                console.log(`Error: ${error}`);
                context.commit('setError', 'Error when loading data!');
            }
        },
        updateRefresh(context, payload) {
            const loadingTime = {
                '1': ['5000'],
                '2': ['10000'],
                '3': ['15000']
            }
            context.commit('setRefresh', loadingTime[payload])
        }
    }
}

export default marketModule;