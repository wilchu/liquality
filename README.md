# Liquality
## List of components

- Header | props: none |
- Radio | props: none |
- Table | props: tableData | type: Object | required |
- Loader | props: none |

## Libraries used

- Axios - Handling requests
- Vuex - Using Store to handle requests and passing data from and to components
- sass loader && node-sass - Using sass as requested (I'm normally using tailwindcss 🔥)
- vueToaster - Messages at the bottom right corner

## Deployment

- [here] - Live version of the app
- Deployed using 🐳  Docker

   [here]: <https://liquality.wilchu.net>
